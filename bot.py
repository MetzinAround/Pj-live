import discord
import twitchio
from twitchio.ext import commands, eventsub
import config as conf

intents = discord.Intents.default()
intents.message_content = True
client = discord.Client(intents=intents)

esbot = commands.Bot.from_client_credentials(client_id = conf.twitch_client,
                                         client_secret = conf.twitch_secret)
esclient = eventsub.EventSubClient(esbot,
                                   webhook_secret='...',
                                   callback_route='https://example.com')


class Bot(commands.Bot):

    def __init__(self):
        super().__init__(token='...', prefix='!', initial_channels=['channel'])

    async def __ainit__(self) -> None:
        self.loop.create_task(esclient.listen(port=4000))

        try:
            await esclient.subscribe_channel_follows_v2(broadcaster=some_channel_ID, moderator=a_channel_mod_ID)
        except twitchio.HTTPException:
            pass

    async def event_ready(self):
        print('Bot is ready!')


bot = Bot()
bot.loop.run_until_complete(bot.__ainit__())


@esbot.event()
async def event_eventsub_notification_follow(payload: eventsub.ChannelFollowData) -> None:
    print('Received event!')
    channel = bot.get_channel('channel')
    await channel.send(f'{payload.data.user.name} followed woohoo!')

bot.run()

    

#use Twitch API read the notification that I am live (event emitter), if I am live, post a message on Discord. 